package topologies;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;
import org.graphstream.graph.Path;
import org.graphstream.graph.implementations.MultiGraph;
import utils.Utils;

import java.util.Iterator;
import java.util.LinkedList;


/**
 * Created by dnl on 14/06/14.
 */
public class FatTreeTopology implements Topology
{
	private int podsNumber;
	private int edgeLayerNumber;
	private int aggrePerPod;

	private LinkedList<Node> coreLayer;
	private LinkedList<Node> aggregationLayer;
	private LinkedList<Node> edgeLayer;
	private LinkedList<Node> hostLayer;
	private MultiGraph graph;

	public FatTreeTopology(String id, int podsNumber)
	{
		this.podsNumber = podsNumber;
		this.edgeLayerNumber = this.podsNumber / 2;
		this.aggrePerPod = podsNumber/2;

		this.graph = new MultiGraph(id);
		this.graph.addAttribute("ui.stylesheet", "url('https://dl.dropboxusercontent.com/u/26227946/fat_tree_style')");

		this.coreLayer = new LinkedList<>();
		this.aggregationLayer = new LinkedList<>();
		this.edgeLayer = new LinkedList<>();
		this.hostLayer = new LinkedList<>();

		this.createNodes();
		this.makeLinks();

		if(!this.verifyTopology())
		{
			System.out.println("error: topology not consistent with " + podsNumber + "-FatTree");
			System.exit(1);
		}
	}

	public MultiGraph getGraph()
	{
		return this.graph;
	}

	public LinkedList<Node> getHostsList()
	{
		return this.hostLayer;
	}

	public int getHostsNumber()
	{
		return this.hostLayer.size();
	}

	public void updatePathFlow(Path p, Integer delta, Node from, Node to)
	{
		if(delta == 0)
			return;

		Iterator<Edge> edges = p.getEdgeIterator();

		while(edges.hasNext())
		{
			Edge e = edges.next();

			String edgeClass = e.getAttribute("ui.class");
			Integer flow = e.getAttribute("flow");
			Integer capacity = Utils.getLinkCapacity(edgeClass);

			if(flow + delta > capacity)
			{
				String layer = null;
				if(edgeClass.contains("l3")) layer = "Core";
				if(edgeClass.contains("l2")) layer = "Aggregation";

				if(edgeClass.contains("l3") || edgeClass.contains("l2"))
				{
					String error = "Link in " + layer +
					" layer has congested during a flow exchange between " + from.getId() + " and " + to.getId() + ".";
					throw new RuntimeException(error);
				}
			}
			else
				e.setAttribute("flow", flow + delta);

			double aux = flow.doubleValue() + delta.doubleValue();
			double ratio = aux/capacity;
			e.setAttribute("ui.color", ratio);
		}
	}

	public String getName()
	{
		return "FatTree-8";
	}

	private void createNodes()
	{
		// build core nodes
		for(int i = 0; i < Math.pow(this.podsNumber / 2, 2); i++)
		{
			Node n = this.graph.addNode("C" + i);
			n.setAttribute("ui.class", "core");
			this.coreLayer.add(n);
		}

		// build aggregation layer here
		for(int i = 0; i < (this.podsNumber*this.podsNumber)/2; i++)
		{
			Node n = this.graph.addNode("A" + i);
			n.setAttribute("ui.class", "aggre");
			this.aggregationLayer.add(n);
		}

		// build edge layer here
		for(int i = 0; i < (this.podsNumber*this.podsNumber)/2; i++)
		{
			Node n = this.graph.addNode("E" + i);
			n.setAttribute("ui.class", "ed");
			this.edgeLayer.add(n);
		}

		// build hosts here
		for(int i = 0; i < Math.pow(this.podsNumber, 3) / 4;  i++)
		{
			Node n = this.graph.addNode("H" + i);
			n.setAttribute("ui.class", "host");
			this.hostLayer.add(n);
		}
	}
	private void makeLinks()
	{
		// link hosts to edges
		for(int i = 0; i < this.hostLayer.size(); i++)
		{
			Node host = this.hostLayer.get(i);
			Node edge = this.edgeLayer.get(i / edgeLayerNumber);
			Edge e = graph.addEdge(host.getId() + "-" + edge.getId(), host, edge);
			e.setAttribute("capacity", 1000);
			e.setAttribute("weight", 1);
			e.setAttribute("ui.class", "l1");
			e.setAttribute("flow", 0);
		}

		// link edges to aggregation
		for(int i = 0; i < this.edgeLayer.size(); i++)
		{
			Node edge = this.edgeLayer.get(i);

			// each edge will link to leftToLink aggre, this is a safety var
			int leftToLink = this.podsNumber/2;

			int podNumber = i / (this.podsNumber/2);

			// start at beginning of the pod
			for(int j = podNumber*this.podsNumber/2; leftToLink > 0; leftToLink--, j++)
			{
				Node aggre = this.aggregationLayer.get(j);
				Edge e = graph.addEdge(edge.getId() + "-" + aggre.getId(), edge, aggre);
				e.setAttribute("capacity", 5000);
				e.setAttribute("weight", 1);
				e.setAttribute("ui.class", "l2");
				e.setAttribute("flow", 0);
			}
		}

		// aggregation to core
		for(int i = 0; i < this.aggregationLayer.size(); i++)
		{
			Node aggre = this.aggregationLayer.get(i);
			int leftToLink = this.podsNumber/2;

			int podIndex = i % this.aggrePerPod;

			for(int j = podIndex * this.aggrePerPod; leftToLink > 0; j++, leftToLink--)
			{
				Node core = this.coreLayer.get(j);
				Edge e = graph.addEdge(aggre.getId() + "-" + core.getId(), aggre, core);
				e.setAttribute("capacity", 10000);
				e.setAttribute("weight", 1);
				e.setAttribute("ui.class", "l3");
				e.setAttribute("flow", 0);
			}
		}
	}
	private boolean verifyTopology()
	{

		if(this.coreLayer.size() != Math.pow(this.podsNumber/2,2))
		{
			System.out.println("error: wrong coreLayer number");
			return false;
		}

		if(this.aggregationLayer.size() != (this.podsNumber*this.podsNumber)/2)
		{
			System.out.println("error: wrong aggregationLayer number");
			return false;
		}

		if(this.edgeLayer.size() != (this.podsNumber*this.podsNumber)/2)
		{
			System.out.println("error: wrong edgeLayer number");
			return false;
		}

		if(this.hostLayer.size() != Math.pow(this.podsNumber, 3) / 4)
		{
			System.out.println("error: wrong hostLayer number");
			return false;
		}

		// check core layer
		for(Node n: this.coreLayer)
		{
			if(n.getDegree() != this.podsNumber)
			{
				System.out.println("error: core layer degree error");
				return false;
			}

			Iterator<Node> it = n.getNeighborNodeIterator();
			while(it.hasNext())
			{
				Node neighbour = it.next();
				String nodeClass = neighbour.getAttribute("ui.class");

				if(nodeClass.compareTo("aggre") != 0)
				{
					System.out.println("error: core layer connection error");
					return false;
				}

			}
		}

		// check aggre layer
		for(Node n: this.aggregationLayer)
		{
			if(n.getDegree() != this.podsNumber)
			{
				System.out.println("error: aggregation layer degree error");
				return false;
			}

			Iterator<Node> it = n.getNeighborNodeIterator();
			int edgeCount = 0;
			int coreCount = 0;

			while(it.hasNext())
			{
				Node neighbour = it.next();
				String nodeClass = neighbour.getAttribute("ui.class");

				if(nodeClass.compareTo("core") == 0)
					coreCount++;
				else if(nodeClass.compareTo("ed") == 0)
					edgeCount++;
				else
				{
					System.out.println(nodeClass);
					System.out.println("error: aggregation layer linked with host layer");
					return false;
				}
			}

			if(edgeCount != this.podsNumber / 2 || coreCount != this.podsNumber / 2)
			{
				System.out.println("error: aggregation layer count error");
				return false;
			}
		}

		// check edge layer
		for(Node n: this.edgeLayer)
		{
			if(n.getDegree() != this.podsNumber)
			{
				System.out.println("error: edge layer degree error");
				return false;
			}

			Iterator<Node> it = n.getNeighborNodeIterator();
			int edgeCount = 0;
			int hostCount = 0;

			while(it.hasNext())
			{
				Node neighbour = it.next();
				String nodeClass = neighbour.getAttribute("ui.class");

				if(nodeClass.compareTo("host") == 0)
					hostCount++;
				else if(nodeClass.compareTo("aggre") == 0)
					edgeCount++;
				else
				{
					System.out.println("error: edge layer linked with core layer");
					return false;
				}
			}

			if(edgeCount != this.podsNumber / 2 || hostCount != this.podsNumber / 2)
			{
				System.out.println("error: edge layer count error");
				return false;
			}
		}

		// check host layer
		for(Node n: this.hostLayer)
		{
			if(n.getDegree() != 1)
			{
				System.out.println("error: host layer degree error " + n.getDegree());
				return false;
			}

			Iterator<Node> it = n.getNeighborNodeIterator();
			while(it.hasNext())
			{
				Node neighbour = it.next();
				String nodeClass = neighbour.getAttribute("ui.class");

				if(nodeClass.compareTo("ed") != 0)
				{
					System.out.println("error: host layer connection error");
					return false;
				}

			}
		}
		return true;
	}


}
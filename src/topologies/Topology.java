package topologies;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.Path;

import java.util.List;


/**
 * Created by dnl on 14/06/14.
 */
public interface Topology
{
	public Graph getGraph();
	public List<Node> getHostsList();
	public int getHostsNumber();
	public void updatePathFlow(Path p, Integer delta, Node from, Node to);
	public String getName();
}

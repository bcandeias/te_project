package experiments;

import algorithms.RoutingAlgorithm;
import matrices.TrafficMatrix;
import org.graphstream.graph.Node;
import org.graphstream.graph.Path;
import topologies.Topology;

import java.util.List;
import java.util.Random;


/**
 * Created by dnl on 14/06/14.
 */
public class Experiment
{
	private Topology network;
	private RoutingAlgorithm routingAlg;
	private TrafficMatrix matrix;
	private double iterations;
	private Random rand;

	public Experiment(Topology top, RoutingAlgorithm alg, TrafficMatrix tm)
	{
		this.network = top;
		this.routingAlg = alg;
		this.iterations = 0;
		this.matrix = tm;
		this.rand = new Random(System.currentTimeMillis());
	}

	public void startExperiment() throws RuntimeException, InterruptedException
	{
		List<Node> hosts = this.network.getHostsList();

		for(;;)
		{
			for(int i = 0; i < hosts.size(); i++)
			{
				Node from = hosts.get(i);

				for(int j = hosts.size()-1; j > 0; j--)
				{
					if(i == j)
						continue;

					Node to = hosts.get(j);

					Path p = this.routingAlg.pickPath(from, to);
					Thread.sleep(10);

					// em MBytes
					this.network.updatePathFlow(p, this.matrix.getTraffic(i, j), from, to);
					this.iterations++;
				}
			}
		}
	}

	public void startRandomExperiment() throws InterruptedException
	{
		List<Node> hosts = this.network.getHostsList();
		Node from, to;
		int fromIndex, toIndex;

		for(;;)
		{
			fromIndex = this.getRandomHost(hosts);
			do
			{
				toIndex = this.getRandomHost(hosts);
			} while(fromIndex == toIndex);


			from = hosts.get(fromIndex);
			to = hosts.get(toIndex);
			Thread.sleep(10);

			Path p = this.routingAlg.pickPath(from, to);
			this.network.updatePathFlow(p, this.matrix.getTraffic(fromIndex, toIndex), from, to);
			this.iterations++;
		}

	}

	private int getRandomHost(List<Node> hosts)
	{
		return this.rand.nextInt(hosts.size());
	}

	public double getIterationsCount()
	{
		return this.iterations;
	}

	public Topology getTopology()
	{
		return this.network;
	}


	public String getTopoloyName()
	{
		return this.network.getName();
	}

	public String getRoutingAlgorithmName()
	{
		return this.routingAlg.getName();
	}

	public String getMatrixName()
	{
		return this.matrix.getName();
	}
}

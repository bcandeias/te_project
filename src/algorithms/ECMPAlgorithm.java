package algorithms;

import org.graphstream.algorithm.Dijkstra;
import org.graphstream.graph.Node;
import org.graphstream.graph.Path;
import topologies.Topology;

import java.util.*;


/**
 * Created by dnl on 13/06/14.
 */
public class ECMPAlgorithm implements RoutingAlgorithm
{
	private HashMap <String, HashMap <String, LinkedList<Path>>> rip;
	private Topology network;
	private Random random;

	public ECMPAlgorithm (Topology topology)
	{
		this.rip = new HashMap<>();
		this.network = topology;
		this.random = new Random(System.currentTimeMillis());

		this.buildRoutingTable();
	}

	private void buildRoutingTable()
	{
		List<Node> hosts = this.network.getHostsList();

		Dijkstra dijkstra = new Dijkstra(Dijkstra.Element.EDGE, null, "weight");
		dijkstra.init(this.network.getGraph());

		for(int i = 0; i < hosts.size(); i++)
		{
			Node source = hosts.get(i);
			HashMap<String, LinkedList<Path>> sourceMap = new HashMap<>();

			for(int j = 0; j < hosts.size(); j++)
			{
				Node destination = hosts.get(j);
				LinkedList<Path> pathsToDestination = this.computePahtsBetween(source, destination, dijkstra);

				sourceMap.put(destination.getId(), pathsToDestination);
			}
			this.rip.put(source.getId(), sourceMap);
		}
	}

	private LinkedList<Path> computePahtsBetween(Node source, Node dest, Dijkstra dij)
	{
		LinkedList<Path> paths = new LinkedList<>();

		dij.setSource(source);
		dij.compute();
		Iterator<Path> allShortestPaths = dij.getAllPathsIterator(dest);

		while(allShortestPaths.hasNext())
			paths.add(allShortestPaths.next());

		return paths;
	}

	public Path pickPath(Node from, Node to)
	{
	    LinkedList<Path> allPaths = rip.get(from.getId()).get(to.getId());

		int rand = this.random.nextInt(allPaths.size());
		return allPaths.get(rand);
	}

	public String getName()
	{
		return "ECMP";
	}

}

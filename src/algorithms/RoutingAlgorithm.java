package algorithms;

import org.graphstream.graph.Node;
import org.graphstream.graph.Path;


/**
 * Created by dnl on 14/06/14.
 */
public interface RoutingAlgorithm
{
	public Path pickPath(Node from, Node to);
	public String getName();
}

package utils;

/**
 * Created by dnl on 14/06/14.
 */
public class Utils
{
	private static int L1_CAPACITY = 1000;
	private static int L2_CAPACITY = 10000;
	private static int L3_CAPACITY = 40000;

	public static int MAX_FLOW_PER_HOST = 80;
	public static int UNIFORM_BASE_FLOW = 40;

	public static int getLinkCapacity(String edgeClass)
	{
		if(edgeClass.compareTo("l1") == 0) return L1_CAPACITY;
		if(edgeClass.compareTo("l2") == 0) return L2_CAPACITY;
		if(edgeClass.compareTo("l3") == 0) return L3_CAPACITY;

		return 0;
	}
}

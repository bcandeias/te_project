package matrices;

import utils.Utils;


/**
 * Created by dnl on 14/06/14.
 */
public class UniformMatrix implements TrafficMatrix
{
	int matrix[][];
	int hostNumber;

	public UniformMatrix(int hostNumber)
	{
		this.matrix = new int[hostNumber][hostNumber];
		this.hostNumber = hostNumber;
		this.resetMatrix();
	}

	@Override
	public int getTraffic(int source, int dest)
	{
		return this.matrix[source][dest];
	}

	@Override
	public void resetMatrix()
	{
		for(int i = 0; i < hostNumber; i++)
		{
			for(int j = 0; j < hostNumber; j++)
				matrix[i][j] = Utils.UNIFORM_BASE_FLOW;
		}
	}


	public String getName()
	{
		return "Uniform";
	}

}

package matrices;

import utils.Utils;
import java.util.Random;


/**
 * Created by dnl on 14/06/14.
 */
public class RandomMatrix implements TrafficMatrix
{

	int matrix[][];
	int hostNumber;

	public RandomMatrix(int hostNumber)
	{
		this.matrix = new int[hostNumber][hostNumber];
		this.hostNumber = hostNumber;

		this.resetMatrix();
	}

	@Override
	public int getTraffic(int source, int dest)
	{
		return this.matrix[source][dest];
	}

	@Override
	public void resetMatrix()
	{
		Random random = new Random(System.currentTimeMillis());

		for(int i = 0; i < this.hostNumber; i++)
		{
			for(int j = 0; j < this.hostNumber; j++)
				matrix[i][j] = random.nextInt(Utils.MAX_FLOW_PER_HOST);
		}
	}

	public String getName()
	{
		return "Random";
	}

}

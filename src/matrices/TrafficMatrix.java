package matrices;

/**
 * Created by dnl on 14/06/14.
 */
public interface TrafficMatrix
{

	public int getTraffic(int source, int dest);
	public void resetMatrix();
	public String getName();

}

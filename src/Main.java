import algorithms.ECMPAlgorithm;
import algorithms.RoutingAlgorithm;
import experiments.Experiment;
import matrices.RandomMatrix;
import matrices.TrafficMatrix;
import matrices.UniformMatrix;
import org.apache.commons.cli.*;
import topologies.FatTreeTopology;
import topologies.Topology;


/**
 * Created by dnl on 14/06/14.
 */
public class Main
{

	public static void main(String args[]) throws InterruptedException, ParseException
	{
		CommandLineParser parser = new BasicParser();

		Options options = new Options();

		Option opAlg = new Option("a", true, "routing algorithm [ecmp]");

		Option opTop = new Option("t", true, "network topology [fattree]");

		Option opTraffic = new Option("m", true, "traffic matrix [uniform, random]");

		Option opRandomHosts = new Option("r", false, "wether the experiment should run picking hosts randomly. " +
				"Default value is false");
		opRandomHosts.setArgName("CENAS");

		Option opHelp = new Option("h", false, "print this message");
		opHelp.setOptionalArg(true);

		options.addOption(opAlg);
		options.addOption(opTop);
		options.addOption(opTraffic);
		options.addOption(opRandomHosts);
		options.addOption(opHelp);

		CommandLine line = null;
		RoutingAlgorithm alg = null;
		TrafficMatrix tm = null;
		Topology topology = null;
		boolean randomHosts = false;

		try
		{
			// parse the command line arguments
			line = parser.parse(options, args);
			if(line.hasOption("h") ||
					!line.hasOption("m") || !line.hasOption("a")
					|| !line.hasOption("t"))
			{
				HelpFormatter helper = new HelpFormatter();
				helper.printHelp(" ", options);
				System.exit(0);
			}
		} catch(ParseException exp)
		{
			System.err.println("Parsing failed.  Reason: " + exp.getMessage());
		}

		String algorithm = line.getOptionValue("a");
		String topologyType = line.getOptionValue("t");
		String traffixMatrix = line.getOptionValue("m");

		if(topologyType.compareTo("fattree") == 0)
			topology = new FatTreeTopology("FatTree-8", 8);
		else
		{
			System.err.println("error: topology not found");
			System.exit(0);
		}

		if(algorithm.compareTo("ecmp") == 0)
			alg = new ECMPAlgorithm(topology);
		else
		{
			System.err.println("error: routing algorithm not found");
			System.exit(0);
		}

		if(traffixMatrix.compareTo("random") == 0)
			tm = new RandomMatrix(topology.getHostsNumber());
		else if(traffixMatrix.compareTo("uniform") == 0)
			tm = new UniformMatrix(topology.getHostsNumber());
		else
		{
			System.err.println("error: traffic matrix not found");
			System.exit(0);
		}

		if(line.hasOption("-r"))
			randomHosts = true;


		Experiment exp = new Experiment(topology, alg, tm);

		System.out.printf("\nExperiment setup:\n" +
				"\nTopology: %s\n" +
				"Routing Algorithm: %s\n" +
				"Traffix Matrix: %s\n" +
				"Random hosts: %b\n", exp.getTopoloyName(), exp.getRoutingAlgorithmName(), exp.getMatrixName(), randomHosts);

		exp.getTopology().getGraph().display();
		Thread.sleep(3000);

		try

		{
			if(randomHosts)
				exp.startRandomExperiment();
			else
				exp.startExperiment();
		} catch(RuntimeException e)

		{
			System.out.println(e.getMessage());
			System.out.println("Iteration count: " + exp.getIterationsCount());
		}
	}
}
